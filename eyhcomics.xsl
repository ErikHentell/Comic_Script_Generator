<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:EYHComicXML="uniqueid.local">

	<xsl:template match="/">
		<html>
			<head>
				<title>Title: <xsl:value-of select="/Issue/Boilerplate/Title"/></title>
				<link rel="stylesheet" href="eyhcomics.css"/>
			</head>
			<body>
				<xsl:for-each select="/Issue/Page">
					<div class="page">
						<xsl:call-template name="Boilerplate"/>

						<h1>PAGE</h1>

						<xsl:for-each select="Panel">
							<div class="panel">
								<p class="panelcount"></p>
								<xsl:for-each select="Description">
									<p class="scenedesc"><xsl:value-of select="text()"/></p>
								</xsl:for-each>

								<xsl:for-each select="Dialogue">
									<div class="dialogueblock">
										<p class="character"><xsl:value-of select="./Character/text()"/></p>
										<p class="talk"><xsl:value-of select="./Talk/text()"/></p>
									</div>
								</xsl:for-each>

								<xsl:for-each select="Caption">
									<div class="captionblock">
										<p class="capttitle"><xsl:value-of select="./Captitle/text()"/></p>
										<p class="capcontent"><xsl:value-of select="./Capcontent/text()"/></p>
									</div>
								</xsl:for-each>

								<xsl:for-each select="SFXBlock">
									<div class="sfxblock">
										<p class="sfxtitle"><xsl:value-of select="./SFXtitle/text()"/></p>
										<p class="sfxcontent"><xsl:value-of select="./SFXcontent/text()"/></p>
									</div>
								</xsl:for-each>

								<!--
								<xsl:for-each select="Character">
									<p class="character"><xsl:value-of select="text()"/></p>
								</xsl:for-each>
									
								<xsl:for-each select="Talk">
									<p class="dialogue"><xsl:value-of select="text()"/></p>
								</xsl:for-each>

								<xsl:for-each select="Cap">
									<p class="captiontitle">Caption</p>
									<p class="captioncontent"><xsl:value-of select="text()"/></p>
								</xsl:for-each>

								<xsl:for-each select="SFXTitle">
									<p class="sfxtitle"><xsl:value-of select="text()"/></p>
								</xsl:for-each>

								<xsl:for-each select="SFXContent">
									<p class="sfxcontent"><xsl:value-of select="text()"/></p>
								</xsl:for-each> -->
							</div>
						</xsl:for-each>
					</div>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>

	<xsl:template name="Boilerplate">
		<table>
			<tr>
				<td class="leftmost"><xsl:value-of select="/Issue/Boilerplate/Title"/></td>
				<td class="midmost">Issue <xsl:value-of select="/Issue/Boilerplate/Number"/></td>
				<td class="midmost">Author: <xsl:value-of select="/Issue/Boilerplate/Author"/></td>
				<td class="rightmost">Copyright &#x000A9; <xsl:value-of select="/Issue/Boilerplate/Copyright"/></td>
			</tr>
		</table>
	</xsl:template>

</xsl:stylesheet>
