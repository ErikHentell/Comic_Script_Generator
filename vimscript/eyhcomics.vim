" Comics.vim
" Define commands for comics writing
" Author: Erik Hentell
" Version: 1.0
" Last Change: 2020-11-08

function! EYHCBoilerplate(title, issue, author, copyright)
	execute "normal! i<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<?xml-stylesheet type=\"text/xsl\" href=\"eyhcomics.xsl\"?>\n\n"

	execute "normal! i<!DOCTYPE Issue SYSTEM \"eyhcomics.dtd\">\n\n"

	execute "normal! i<Issue>\n\n"
	
	execute "normal! i<Boilerplate>\n\t<Title>" . a:title . "</Title>\n\t<Number>" . a:issue . "</Number>\n\t<Author>" . a:author . "</Author>\n\t<Copyright>" . a:copyright . "</Copyright>\n</Boilerplate>\n\n"
	execute "normal! i<Page>\n\n<Panel>\n"
endfunction

function! EYHCDesc()
	execute "normal! i<Description>\nDESCTAG\n</Description>\n"
	call search("DESCTAG")
	normal! dw
endfunction	

function! EYHCDialogue(name)
	execute "normal! i<Dialogue>\n\t<Character>"
	execute "normal! a" . a:name
	execute "normal! a</Character>\n"

	execute "normal! i\t<Talk>\nTALKTAG\n</Talk>\n<Dialogue>\n\n"
	call search("TALKTAG")
	normal! dw
endfunction

function! EYHCCaption()
	execute "normal! i<Caption>\n\t<Captitle>Caption</Captitle>\n\t"
	execute "normal! i\n\t<Capcontent>\nCAPTAG\n</Capcontent>\n</Caption>\n\n"
	call search("CAPTAG")
	normal! dw
endfunction

function! EYHCSFX(name)
	execute "normal! i<SFXBlock>\n\t<SFXtitle>" . a:name . "</SFXtitle>\n\t"
	execute "normal! i\n\t<SFXcontent>\nSFXTAG\n</SFXcontent>\n</SFXBlock>\n\n"
	call search("SFXTAG")
	normal! dw
endfunction

function! EYHCNext(opt)
	if a:opt == 0
		execute "normal! i\n</Panel>\n\n<Panel>\n"
	elseif a:opt == 1
		execute "normal! i\n</Panel>\n\n</Page>\n\n<Page>\n\n<Panel>\n"
		normal! g
	elseif a:opt == 2
		execute "normal! i\n</Panel>\n\n</Page>\n\n</Issue>"
	endif
endfunction
