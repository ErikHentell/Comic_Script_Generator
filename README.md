# Comic_Script_Generator

Tools for creating a comic book script using XML, XSLT, and a web browser to export a PDF

**Background**
==============
This project was created when I began to ponder a way to create scripts of any type using a standardized file format. The ultimate goal would be to make a machine-readable format for various uses. For example, assembling a large quantity of such scripts and running them through an AI system to see if it can generate a passable work of its own. If this were possible, it would have enormous implications for the generation of fictional works.

I chose a comic book script as the first type of script to create because there is no defined format for comic book scripts. In recent years, more screenwriters have been coming into the field, so scripts have been centering on something close to a screenplay, but overall there is still no official standard. With that in mind, I decided to come up with my own format using XML, XSLT and CSS for the final HTML file.

This system requires a browser or other application that can understand XSLT and transform the XML into an HTML format. For this, I used Firefox, but Firefox has a safety measure that prevents the use of XSLT from a file system. As a result, the XML, XSLT and CSS files must be on a server to be read properly. I don't know if other browsers have this limitation as well.

**TODO**
========
* Create a sample script to demonstrate the use of the XML and XSLT system
* Plan a JavaScript front-end that can read the XML and construct a visual system for manipulating and saving out a new XML file
* Possibly create a CGI script to communicate with the JavaScript system
